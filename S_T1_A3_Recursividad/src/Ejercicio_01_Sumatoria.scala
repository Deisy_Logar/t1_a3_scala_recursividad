/*
 			Ejercicio 01:
 					Programa que muestre la sumatoria de n�meros enteros desde un l�mite inicial hasta un l�mite final.
 */


object Ejercicio_01_Sumatoria {
  
  def sumatoriaLimiteRecursiva(limiteInicial : Int, limiteFinal : Int) : Int = {
    
    var sumatoria: Int = 0
    
    if(limiteInicial > limiteFinal) {
      sumatoria = 0
      
    } else {
      sumatoria = limiteInicial + sumatoriaLimiteRecursiva((limiteInicial + 1), limiteFinal)
      
    }
    
    sumatoria
  }
  
  def main(args: Array[String]): Unit = {
    
    println("\t SUMATORIA")
    
    println("\n Sumatoria = " + sumatoriaLimiteRecursiva(34, 68))
    
  }
  
}