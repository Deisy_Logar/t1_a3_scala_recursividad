import scala.io.StdIn._

object Ejercicio_02_Divisores {
  
  def divisores(num: Int, numAuxiliar: Int) : Int = {
    
    if(numAuxiliar < 1){
      0
    }else{
      if((num % numAuxiliar) == 0){
        print(numAuxiliar + ", ")
      }
      divisores(num, (numAuxiliar - 1))
    }
    
  }
  
  def main(args: Array[String]): Unit = {
    
    println("\t Divisores de un N�mero")
    
    println("\n Ingrese un n�mero: ")
    var num = readInt()
    
    println("\n Los n�meros divisorres son:\n")
    
    divisores(num, num)
  }
  
}