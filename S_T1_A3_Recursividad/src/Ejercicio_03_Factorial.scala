import scala.io.StdIn._

object Ejercicio_03_Factorial {
  
  def factorialNumero(num: Int): Int = {
    
    var factorial: Int = 0
    
    if(num == 0){
      factorial = 1
    }else{
      factorial = num * factorialNumero(num - 1)
    }
    
    factorial
    
  }
  
  def main(args: Array[String]): Unit = {
    
    println("\n \t Factorial de un N�mero")
    
    println("Ingrese un n�mero: ")
    var num = readInt()
    
    println("\n Factorial = " + factorialNumero(num))
    
  }
  
}